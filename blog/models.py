from django.db import models
from django.contrib.auth.models import User


class Category(models.Model):
    title = models.CharField(max_length=128, db_index=True)
    description = models.CharField(max_length=256)
    slug = models.SlugField(max_length=128, db_index=True)

    def __unicode__(self):
        return self.title

    def __str__(self):
        return self.title


class Tag(models.Model):
    name = models.CharField(max_length=32)

    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.name


class Article(models.Model):
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    tags = models.ManyToManyField(Tag, related_name="articles")

    title = models.CharField(max_length=128)
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    content = models.TextField()
    slug = models.SlugField(max_length=128, unique=True)
    published = models.BooleanField(default=False)
    description = models.CharField(max_length=256, default="")

    creation_date = models.DateTimeField('date created', db_index=True)
    publish_date = models.DateTimeField('date published', db_index=True)
    updated_date = models.DateTimeField('date updated', db_index=True)

    def __unicode__(self):
        return self.title

    def __str__(self):
        return self.title
