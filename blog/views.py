from django.shortcuts import render, get_object_or_404
from .models import Article, Category

import markdown

# use this to get a short blurb from a block of text
def get_blurb(content, max_word_count=50, average_word_char=7):
    max_char_count = max_word_count * average_word_char

    space_count = 0
    char_count = 0
    for c in content:
        if c == " ":
            space_count += 1
        if space_count >= max_word_count:
            # using spaces to count words
            return content[:char_count]
        if char_count == max_char_count:
            # prevents from parsing for too long
            return content[:char_count] + "-"
        char_count += 1

    # if max_word_count was larger than the given text
    return content


def index(request):
    max_article_list_len = 10
    article_list = []

    for article in Article.objects.order_by('-publish_date'):
        if not article.published:
            continue

        article.content = get_blurb(article.content)
        article_list.append(article)

        if len(article_list) >= max_article_list_len:
            break
    return render(request, "blog/index.html", {"article_list": article_list})


def post(request, article_slug):
    article = get_object_or_404(Article, slug__exact=article_slug, pk__gt=0)
    article.content = markdown.markdown(
        article.content,
        extensions=[
            'markdown.extensions.codehilite',
            'markdown.extensions.fenced_code',
            'markdown.extensions.sane_lists',
        ]
    )
    context = {'article': article}
    return render(request, 'blog/article.html', context)


def category(request, category_slug):
    category = get_object_or_404(Category, slug__exact=category_slug, pk=1)
    return render(request, 'blog/category.html', {'category': category})
