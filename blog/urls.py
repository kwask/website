from django.urls import path

from . import views

app_name = "blog"

urlpatterns = [
    path("", views.index, name="index"),
    path("articles/<slug:article_slug>/", views.post, name='article'),
    path("categories/<slug:category_slug>/", views.category, name='category'),
]
