from django.db import models
from django.contrib.auth.models import User
from blog.models import Category, Tag

# Create your models here.

class Project(models.Model):
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    tags = models.ManyToManyField(Tag, related_name="projects")

    title = models.CharField(max_length=128)
    slug = models.SlugField(max_length=128, unique=True)
    published = models.BooleanField(default=False) # visible or not
    description = models.TextField() # shorter description 
    content = models.TextField(default="") # longer description shown on project page

    creation_date = models.DateTimeField('date created', db_index=True)
    publish_date = models.DateTimeField('date published', db_index=True)
    updated_date = models.DateTimeField('date updated', db_index=True)

    def __unicode__(self):
        return self.title

    def __str__(self):
        return self.title


class Link(models.Model):
    name = models.CharField(max_length=128)
    link = models.CharField(max_length=2083)
    project = models.ForeignKey(Project, on_delete=models.CASCADE)

    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.name
