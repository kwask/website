from django.shortcuts import render
from .models import Project, Link

# Create your views here.

def index(request):
    max_list_len = 10
    project_list = []

    for project in Project.objects.order_by('-creation_date'):
        if not project.published:
            continue
        project.links = Link.objects.filter(project__exact=project)
        project_list.append(project)
        if len(project_list) >= max_list_len:
            break

    return render(request, "projects/index.html", {"project_list": project_list})
