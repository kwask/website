from django.urls import include, path

from . import views

app_name = "personal"

urlpatterns = [
    path("", views.index, name="index"),
    path("resume/", views.resume, name="resume"),
    path("404/", views.error_404, name="404"),
    path("50x/", views.error_50x, name="50x"),
]
