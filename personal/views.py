from django.http import Http404
from django.shortcuts import render


def index(request):
    return render(request, "personal/index.html")


def links(request):
    return render(request, "personal/links.html")


def resume(request):
    return render(request, "personal/resume.html")


def error_404(request):
    return render(request, "404.html")


def error_50x(request):
    return render(request, "50x.html")
